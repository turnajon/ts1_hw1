package cz.cvut.fel.ts1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class turnajonTest {
    @Test
    public void testFactorial (){
        turnajon turnajon = new turnajon();
        long factorial5 = turnajon.factorial(5);
        Assertions.assertEquals(120, factorial5);
        long factorial0 = turnajon.factorial(0);
        Assertions.assertEquals(1,factorial0);
        long factorial1 = turnajon.factorial(1);
        Assertions.assertEquals(1, factorial1);
    }
}
