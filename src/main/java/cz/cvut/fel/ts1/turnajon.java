package cz.cvut.fel.ts1;

public class turnajon {
    public long factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}
